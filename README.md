# snake-python
Snake game in terminal, created with python and python-curses.

# How to run
Either of the following:
- ```python3 snake.py```
- ```python snake.py```
- ```./snake.py```
