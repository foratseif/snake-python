#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Forat Al-Hellali <foratseif@gmail.com>
# Desc: Snake game inside the terminal
# Started: 3.sept.16 20:41
# Done:    4.sept.16 05:36
# Reason for creating: No Life..

import curses, time, random
import sys

UPPER='▀'; WHOLE='█'; UNDER='▄'
LEFT=0; RIGHT=1; UP=2; DOWN=3

height=0
width=0
stdscr=None

# init the game window
def init():
    global stdscr, height, width
    stdscr = curses.initscr()
    curses.curs_set(0)
    stdscr.keypad(True)

    height, width = stdscr.getmaxyx()
    height *= 2

# end the game window
def end():
    if stdscr:
        curses.endwin()

# addstr that avoids error when printing in bottom right corner
def _addstr(y, x, val):
    try:
        stdscr.addstr(int(y/2), x, val)
    except curses.error:
        stdscr.move(0,0)

# render
def render(render, clear=[]):

    # clear boxes from last run
    for y, x in clear:
        _addstr(y, x, " ")

    # print new boxes
    for y, x in render:
        if not y%2:
            val = UPPER
            if (y+1, x) in render:
                val = WHOLE
        else:
            val = UNDER
            if (y-1, x) in render:
                val = WHOLE
        _addstr(y, x,val)

    # output snake
    stdscr.refresh()

# move snake head
def move(head, d):
    head = list(head)
    if d == RIGHT:
        head[1]+=1
    elif d == LEFT:
        head[1]-=1
    elif d == UP:
        head[0]-=1
    elif d == DOWN:
        head[0]+=1
    return tuple(head)

# check for crash
def crash(new, old):
    head = new[-1]
    if head in old       : return True # crash with self
    if head[0] < 0       : return True # crash border
    if head[0] >= height : return True # crash border
    if head[1] < 0       : return True # crash border
    if head[1] >= width  : return True # crash border
    return False

# set getch timeout
def set_timeout(to):
    stdscr.timeout(to)

# create snake array
def create_snake(l=5):
    h = int(height/2)
    w = int(width/2)
    snake = [(h-1, w-l)]
    for i in range(1,l):
        snake.append((snake[i-1][0], snake[i-1][1]+1))
    return snake

# create food coordinates
def create_food():
    y = random.randint(0, height-1)
    x = random.randint(0, width-1)
    return y, x

# main run function for the game
def run(slen=10, to=55, to_min=35, to_step=2, lvl_step=3):

    # init vars
    snake = create_snake(l=slen)
    food  = create_food()
    direc = RIGHT
    set_timeout(to)
    last_len = slen

    won = False
    while True:

        old = snake[:]
        tmp = snake[:]
        snake[-1] = move(snake[-1], direc) # move head
        snake[:-1] = tmp[1:]               # move body

        # check crash
        if crash(snake, old):
            won = False
            break

        # check food eaten
        if food in snake:
            food = create_food()
            snake[:-1] = tmp[:] # dont move the bodyy

            # increase snake speed
            if len(snake) > last_len + lvl_step:
                if to > to_min:
                    to -= to_step
                    set_timeout(to)

        # re-render everything
        render(snake+[food], old)

        # listen for keypress
        direc = keyboard(direc)

        # check if the user won
        if len(snake) == width*height:
            won = True
            break

    # return stats
    return won, len(snake), int(1/(float(to)/1000.000))


# listen for keypress
def keyboard(old_direc):
    c = stdscr.getch()
    if c == curses.KEY_UP and old_direc != DOWN:
        return UP
    elif c == curses.KEY_DOWN and old_direc != UP:
        return DOWN
    elif c == curses.KEY_LEFT and old_direc != RIGHT:
        return LEFT
    elif c == curses.KEY_RIGHT and old_direc != LEFT:
        return RIGHT
    return old_direc


# start the main program
if __name__ == '__main__':
    # check python version
    if sys.version_info[0] < 3:
        import locale
        locale.setlocale(locale.LC_ALL,"") #compensate for utf8 chars

    #parse arguments
    debug_mode = ('--debug' in sys.argv)

    #import traceback if in debug_mode
    if debug_mode:
        import traceback

    # init the game
    init()

    # run the game
    try:
        s, l, v = run(slen=3, to=52, to_min=40, to_step=2, lvl_step=3)

        if s:
            result  = 'won!'
        else:
            result = 'lost.'
        message = "You "+str(result)+"\nSnake Length: "+str(l)+" units.\nSpeed: "+str(v)+" units/s"
    except KeyboardInterrupt as e:
        message = 'You quit.'
    except:
        end()

        if debug_mode:
            traceback.print_exc()
        else:
            print("The game crashed :/")
        exit(1)

    # end curses
    end()

    print(message)
